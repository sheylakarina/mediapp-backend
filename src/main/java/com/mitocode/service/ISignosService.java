package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Signos;

public interface ISignosService {
	void registrar(Signos signos);
	void modificar(Signos signos);
	Signos listarId(int idSignos);
	List<Signos> listar();
	
}
