package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Examen;
import com.mitocode.model.Paciente;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

@RestController
@RequestMapping("/signos")
public class SignosController {
	@Autowired
	private ISignosService service;
	
	@PostMapping(value="/registrar",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrar(@RequestBody Signos signos){
		int resultado=0;
		System.out.println("signos "+signos.getPaciente().getIdPaciente());
		System.out.println("signos "+signos.getRitmoRespiratorio());
		try {
			service.registrar(signos);
			resultado=1;
		} catch (Exception e) {
			resultado=0;
			//return new ResponseEntity<Integer>(resultado,HttpStatus.INTERNAL_SERVER_ERROR);	
		
		}
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
	}
	
	@PostMapping(value="/actualizar",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> actualizar(@RequestBody Signos signos){
		int resultado=0;
		
		try {
			service.modificar(signos);
			resultado=1;
		} catch (Exception e) {
			resultado=0;
			return new ResponseEntity<Integer>(resultado,HttpStatus.INTERNAL_SERVER_ERROR);	
		
		}
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
	}
		
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Signos>> listar(){
		List<Signos> signos = new ArrayList<>();
		try {
			signos = service.listar();
		}catch(Exception e) {
			return new ResponseEntity<List<Signos>>(signos, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Signos>>(signos, HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Signos> listarId(@PathVariable("id") Integer id) {
		Signos signos = new Signos();
		signos = service.listarId(id);
		return new ResponseEntity<Signos>(signos, HttpStatus.OK);
	}
	
}
