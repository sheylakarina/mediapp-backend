package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Signos;

@Repository
public interface ISignosDAO extends JpaRepository<Signos, Integer> {

}